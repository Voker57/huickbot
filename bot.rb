# encoding: utf-8

require 'rubygems'
require "bundler/setup"
require 'cgi'
require 'active_record'
require 'active_support/dependencies'
require 'blather/client/client'
require 'yaml'
require 'tilt'
require 'action_view'

Dir.chdir CURRENT_DIR if defined? CURRENT_DIR

default_config = { }

$config = default_config.merge(YAML.load(File.read("config.yml")))

PREFIX = $config[:prefix]



module TemplateRenderer
	def self.render(what, locals)
		@templates ||= {}
		@templates[what] ||= Tilt::ERBTemplate.new("templates/#{what}.erb")
		@templates[what].render(nil, locals.merge(:site_url => $config[:site_url])).force_encoding("UTF-8").strip
	end
	
	def self.reset_templates
		@templates = {}
	end
end

module BotLogger
	def self.log(line)
		File.open("#{PREFIX}/log/bot.log", "a") do |f|
			f.puts line
		end
	end
end

ActiveRecord::Base.establish_connection(YAML::load(File.open("#{PREFIX}/config/database.yml"))["production"])
ActiveRecord::Base.logger = Logger.new(STDERR)

ActiveSupport::Dependencies.autoload_paths << "#{PREFIX}/app/models" << "#{PREFIX}/app/helpers" << "#{PREFIX}/lib"

Huick.bot_socket = Dir.pwd + "/bot.sock"

def notify_guys
	begin
	PostRecommendation.where(:notified => false).find_each do |post_recommendation|
		post_recommendation.user.subscribers.where(:banned => false, :offed => false).each do |subscriber|
			next if post_recommendation.post.hidden_by? subscriber
			Huick.acknowledge(post_recommendation.post, subscriber) do
				say Blather::JID.new(subscriber.jid), TemplateRenderer.render("post_recommendation", :recommendation => post_recommendation) if subscriber.jid
			end
		end
		
		if not post_recommendation.user.hidden_by? post_recommendation.post.user and not post_recommendation.post.user.banned? and not post_recommendation.post.user.offed?
			say Blather::JID.new(post_recommendation.post.user.jid), TemplateRenderer.render("post_recommendation_notification", :recommendation => post_recommendation) if post_recommendation.post.user.jid
		end
		
		post_recommendation.notified = true
		if post_recommendation.recommending_comment and post_recommendation.recommending_comment.notified == false
			post_recommendation.recommending_comment.notified = true
			post_recommendation.recommending_comment.save!
		end
		post_recommendation.save!
	end
	
	Comment.where(:notified => false).find_each do |comment|
		comment.post.subscribers.where(:banned => false, :offed => false).each do |subscriber|
			next if subscriber.hidden_users.exists? comment.user.id
			Huick.acknowledge(comment, subscriber) do
				say Blather::JID.new(subscriber.jid), TemplateRenderer.render("comment_notification", :comment => comment) if subscriber.jid
			end
		end
		comment.notified = true
		comment.save!
	end
	
	CommentRecommendation.where(:notified => false).find_each do |comment_recommendation|
		comment_recommendation.user.subscribers.where(:banned => false, :offed => false).each do |subscriber|
			next if subscriber.hidden_users.exists? comment_recommendation.user.id
			Huick.acknowledge(comment_recommendation.comment, subscriber) do
				say Blather::JID.new(subscriber.jid), TemplateRenderer.render("comment_recommendation", :recommendation => comment_recommendation) if subscriber.jid
			end
		end
		
		Huick.acknowledge(comment_recommendation.comment, comment_recommendation.comment.user) do
			if not comment_recommendation.user.hidden_by? comment_recommendation.comment.user and not comment_recommendation.comment.user.banned? and not comment_recommendation.comment.user.offed?
				say Blather::JID.new(comment_recommendation.comment.user.jid), TemplateRenderer.render("comment_recommendation_notification", :recommendation => comment_recommendation) if comment_recommendation.comment.user.jid
			end
		end
		
		comment_recommendation.notified = true
		comment_recommendation.save!
	end
	
	Post.where(:notified => false).find_each do |post|
		subs = (post.user.subscribers.where(:banned => false, :offed => false).to_a + post.tags.map{|tag| tag.subscribers.where(:banned => false, :offed => :false)}.flatten.to_a).uniq
		if not post.private?
			subs.each do |subscriber|
				next if post.hidden_by? subscriber
				Huick.acknowledge(post, subscriber) do
					say Blather::JID.new(subscriber.jid), TemplateRenderer.render("post", :post => post, :user => subscriber) if subscriber.jid
				end
			end
		end
		if post.target_user and not post.hidden_by? post.target_user
			Huick.acknowledge(post, post.target_user) do
				say Blather::JID.new(post.target_user.jid), TemplateRenderer.render("post", :post => post, :user => post.target_user) if post.target_user.jid
			end
		end
		post.notified = true
		post.save!
	end
	
	UserSubscription.where(:notified => false).find_each do |user_subscription|
		next if user_subscription.subscriber.hidden_by? user_subscription.subscribed or user_subscription.subscribed.offed? or user_subscription.subscribed.banned?
		say Blather::JID.new(user_subscription.subscribed.jid), TemplateRenderer.render("subscription_notification", :user_subscription => user_subscription) if user_subscription.subscribed.jid
		user_subscription.notified = true
		user_subscription.save!
	end
	
	rescue Exception => e
		ecode = DateTime.now.strftime("%s")
		File.open("error.log", "a") do |f|
			f.puts "[#{DateTime.now.to_s}] {#{ecode}} Exception #{e} raised at #{e.backtrace.join("    ")}"
		end
	end
end

module Notifier
	def post_init
		self.close_connection
		notify_guys
	end
end

module Throttler
	def self.activity_logs
		@activity_logs ||= {}
	end
	
	def self.activity(user_id)
		timestamp = Time.now.to_i
		activity_logs[user_id] ||= []
		activity_logs[user_id] << timestamp
		activity_logs[user_id].delete_if {|t| t < timestamp - 10}
		return(activity_logs[user_id].size < 4)
	end
	
	def self.activity!(user_id)
		Throttler.activity(user_id) or raise Huick::TooFastError
	end
end

include PostsHelper

=begin
	huick bot
	requires: xmpp4r-simple, activerecord
	:nodoc:
=end

$client = Blather::Client.setup $config[:jid], $config[:password]

$client.register_handler(:ready) do
	notify_guys
	EventMachine.start_unix_domain_server(Huick.bot_socket, Notifier)
end

$client.register_handler :subscription, :request? do |s|
  $client.write s.approve!
end

def say(to, msg)
	$client.write Blather::Stanza::Message.new(to, msg)
end

$client.register_handler :message do |m|
	next unless m.body
	next if m.from.to_s == $config[:jid] # No talking to ourselves
	begin
	Throttler.activity! m.from.stripped.to_s
	BotLogger.log "[#{Time.now}] <#{m.from.to_s}> #{m.body.gsub("\n",'\n')}"
	msg = m.body.strip
	if msg =~ /\Aping\z/im
		say m.from, "PONG"
	elsif msg =~ /\Alogin\z/im
		user = Huick.require_user(m.from.stripped.to_s)
		begin
			user.access_token = Digest::SHA512.hexdigest(open("/dev/urandom").read(1024))
		end while User.where(access_token: user.access_token).first
		user.save!
		say m.from, "#{$config[:site_url]}/login/#{user.access_token}"
	elsif msg =~ /\AS\z/im
		# List subs
		user = Huick.require_user(m.from.stripped.to_s)
		message = ""
		message << "User subscriptions: #{user.subscribed_users.map{|u| "@#{u.username}"}.join(", ")}\n"
		message << "Tags subscriptions: #{user.subscribed_tags.map{|t| t.tag_text}.join(", ")}\n"
		say m.from, message
	elsif msg =~ /\ASUBS\z/im
		# List subs
		user = Huick.require_user(m.from.stripped.to_s)
		message = ""
		message << "Your subscribers: #{user.subscribers.map{|u| "@#{u.username}"}.join(", ")}\n"
		say m.from, message
	elsif msg =~ /\BL\z/im
		# List BLs
		user = Huick.require_user(m.from.stripped.to_s)
		message = ""
		message << "Blacklisted users: #{user.blacklisted_users.map{|u| "@#{u.username}"}.join(", ")}\n"
		say m.from, message
	elsif msg =~ /\AH\z/im
		# List hides
		user = Huick.require_user(m.from.stripped.to_s)
		message = ""
		message << "Hidden users: #{user.hidden_users.map{|u| "@#{u.username}"}.join(", ")}\n"
		message << "Hidden tags: #{user.hidden_tags.map{|t| t.tag_text}.join(", ")}\n"
		say m.from, message
	elsif msg =~ /\AH[AE]LP\z/m
		say m.from, TemplateRenderer.render("help", {})
	elsif msg =~ /\AON\z/m
		user = Huick.require_user(m.from.stripped.to_s)
		user.offed = false
		user.save!
		say m.from, "You're on!"
	elsif msg =~ /\AOFF\z/m
		user = Huick.require_user(m.from.stripped.to_s)
		user.offed = true
		user.save!
		say m.from, "Off you go!"
	elsif msg =~ /\A@([a-zA-Z0-9\-]+)\z/m
		user = User.find_by_username($1) or raise Huick::NoSuchUserError
		
		say m.from, TemplateRenderer.render("user", :user => user)
	elsif msg =~ /\A@([a-zA-Z0-9\-]+)\+\z/m
		user = User.find_by_username($1) or raise Huick::NoSuchUserError
		
		say m.from, TemplateRenderer.render("userplus", :user => user, :posts => user.posts.noprivates.order("created_at DESC").first(10))
	elsif msg =~ /\A#([a-z]+)\z/m
		# Display post
		u = User.find_by_jid(m.from.stripped.to_s)
		
		postid = $1
		pst = Huick.require_post(postid)
		raise Huick::NoPermissionError unless pst.accessible_by? u
		
		say m.from, TemplateRenderer.render("post", :post => pst)
	elsif msg =~ /\A#([a-z]+)\+\z/m
		# Display post with comments
		u = User.find_by_jid(m.from.stripped.to_s)
		
		postid = $1
		pst = Huick.require_post(postid)
		raise Huick::NoPermissionError unless pst.accessible_by? u
		
		say m.from, TemplateRenderer.render("full_post", :post => pst, :user => User.by_jid(m.from.stripped.to_s), :comments => Huick.hide(pst.comments, u))
	elsif msg =~ /\A#([a-z]+)\/(\d+)\z/m
		# Display comment
		u = User.find_by_jid(m.from.stripped.to_s)
		
		postid = $1
		commentnum = $2
		
		pst = Huick.require_post(postid)
	
		cmt = Comment.where(:post_id => pst.id, :number => commentnum.to_i).first or raise Huick::NoSuchCommentError.new
		raise Huick::NoPermissionError unless cmt.accessible_by? u
		
		say m.from, TemplateRenderer.render("comment", :comment => cmt)
	elsif msg =~ /\AS #([a-z]+)\z/m
		# Subcribe to post
		postid = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
		
		pst = Huick.require_post(postid)
		
		raise Huick::NoPermissionError unless pst.accessible_by? u
		
		Huick.subscribe_to_post(u,pst)
		
		say m.from, "Subscribed!"
	elsif msg =~ /\AU #([a-z]+)\z/m
		# Unsubcribe from post
		postid = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
		
		pst = Huick.require_post(postid)
		
		Huick.unsubscribe_from_post(u,pst)
		
		say m.from, "Unsubscribed!"
	elsif msg =~ /\AS \* (.*?)\z/m
		# Subcribe to tag
		tags_text = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
		
		tag = Tag.from_text($1.strip)
		unless u.subscribed_tags.exists? tag.id
			Huick.subscribe_to_tag(u, tag)
		end
		
		say m.from, "Subscribed!"
	elsif msg =~ /\AU \* (.*?)\z/m
		# Unsubcribe from tags
		tags_text = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
		
		tags_text.split(/\s*,\s*/).each do |tag_text|
			tag = Tag.from_text(tag_text)
			if u.subscribed_tags.exists? tag
				Huick.unsubscribe_from_tag(u, tag)
			end
		end
		
		say m.from, "Unsubscribed!"
	elsif msg =~ /\AS @([a-zA-Z0-9\-]+)\z/m
		# Subcribe to user
		username = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
	
		sub_u = User.where(:username => username).first or raise Huick::NoSuchUserError
				
		Huick.subscribe_to_user(u, sub_u)
		
		say m.from, "Subscribed!"
	elsif msg =~ /\AU @([a-zA-Z0-9\-]+)\z/m
		# Unsubcribe from user
		username = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
	
		sub_u = User.where(:username => username).first or raise Huick::NoSuchUserError
				
		Huick.unsubscribe_from_user(u, sub_u)
		
		say m.from, "Unsubscribed!"
	elsif msg =~ /\ABL @([a-zA-Z0-9\-]+)\z/m
		# [un]Blacklist user
		username = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
	
		bl_u = User.where(:username => username).first or raise Huick::NoSuchUserError
		
		if bl_u.blacklisters.exists? u.id
			Huick.unblacklist_user(u, bl_u)
			say m.from, "Unblacklisted!"
		else
			Huick.blacklist_user(u, bl_u)
			say m.from, "Blacklisted!"
		end
	elsif msg =~ /\AH @([a-zA-Z0-9\-]+)\z/m
		# [un]Hide user
		username = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
	
		h_u = User.where(:username => username).first or raise Huick::NoSuchUserError
		
		if h_u.hiders.exists? u.id
			Huick.unhide_user(u, h_u)
			say m.from, "Unhid!"
		else
			Huick.hide_user(u, h_u)
			say m.from, "Hid!"
		end
	elsif msg =~ /\AH \*\s+([^,]+)\z/m
		# [un]Hide tag
		u = Huick.require_user(m.from.stripped.to_s)
		
		tag = Tag.from_text($1.strip)
		
		if tag.hiders.exists? u.id
			Huick.unhide_tag(u, tag)
			say m.from, "Unhid!"
		else
			Huick.hide_tag(u, tag)
			say m.from, "Hid!"
		end
	elsif msg =~ /\A! #([a-z]+)(?:\s+(.*))?\z/m
		# Recommend/Unrecommend post
		postid = $1
		rec_text = $2
		
		u = Huick.require_user(m.from.stripped.to_s)
		
		pst = Huick.require_post(postid)
		raise Huick::PrivatePostError if pst.private?
		
		old_p_rec = PostRecommendation.where(:post_id => pst.id, :user_id => u.id).first
		
		if old_p_rec
			# Unrecommending
			Huick.unrecommend_post(u, pst)
			say m.from, "Unrecommended!"
		else
			# Recommending
			Huick.recommend_post(u, m.from.resource, pst, rec_text)
			# TODO: subscribe to post?
			say m.from, "Recommended!"
		end
	elsif msg =~ /\A! #([a-z]+)\/(\d+)(?:\s+(.*))?\z/m
		# Recommend/Unrecommend comment
		postid = $1
		cmt_num = $2
		rec_text = $3
		
		u = Huick.require_user(m.from.stripped.to_s)
		
		pst = Huick.require_post(postid)
		raise Huick::PrivatePostError if pst.private?
		cmt = pst.comments.where(:number => cmt_num.to_i).first
		
		old_c_rec = CommentRecommendation.where(:comment_id => cmt.id, :user_id => u.id).first
		
		if old_c_rec
			# Unrecommending
			Huick.unrecommend_comment(u, cmt)
			say m.from, "Unrecommended!"
		else
			# Recommending
			Huick.recommend_comment(u, m.from.resource, cmt, rec_text)
			# TODO: subscribe to post?
			say m.from, "Recommended!"
		end
	elsif msg =~ /\AD #([a-z]+(\/\d+)?)\z/m
		# delete post/comment
		
		wid = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
		
		if $2
			# comment
			cmt = Huick.require_comment(wid)
			Huick.delete_comment(u,cmt)
			say m.from, "Deleted comment ##{cmt.cmtid_text}"
		else
			# post
			pst = Huick.require_post(wid)
			Huick.delete_post(u,pst)
			say m.from, "Deleted post ##{pst.postid_text}"
		end
		
	elsif msg =~ /\AD L\z/m
		# delete last post/comment
		u = Huick.require_user(m.from.stripped.to_s)
		
		cmt, pst = u.comments.last, u.posts.last
		
		if cmt == nil and pst == nil
			raise Huick::NoSuchPostError
		end
		if cmt and cmt.created_at > pst.created_at
			Huick.delete_comment(u,cmt)
			say m.from, "Deleted comment ##{cmt.cmtid_text}"
		else
			Huick.delete_post(u,pst)
			say m.from, "Deleted post ##{pst.postid_text}"
		end
	elsif msg =~ /\A#([a-z]+)\s+\*\s+(.*)\z/m
		# Retag
		postid = $1
		
		u = Huick.require_user(m.from.stripped.to_s)
		Huick.require_unbanned(u)
		pst = Huick.require_post(postid)
		
		raise Huick::NoPermissionError unless pst.user == u
		
		tags = $2.strip
		tags.split(/\s*,\s*/).each do |tag_text|
			if tag_text =~ /^!/
				pst.del_tag(tag_text[1..-1])
			else
				pst.add_tag(tag_text[1..-1])
			end
		end
		
		pst.save!
		say m.from, "Tags updated: #{pst.tags.map{|v| v.tag_text}.join(", ")}"
	elsif msg =~ /\A#([a-z]+)(?:\/(\d+))?\s+(.*)\z/m
		# Comment!
		postid = $1
		comment_num = $2
		comment_text = $3
		
		u = Huick.require_user(m.from.stripped.to_s)
		
		pst = Huick.require_post(postid)
		
		parent_comment = nil
		
		if comment_num
			parent_comment = Comment.where(:post_id => pst.id, :number => comment_num.to_i).first or raise Huick::NoSuchCommentError
		end
		
		cmt = Huick.add_comment(u, m.from.resource, pst,  comment_text, parent_comment)	
		
		say m.from, "Comment ##{cmt.cmtid_text}: #{$config[:site_url]}/p/#{cmt.post.postid_text}##{cmt.number}"
	elsif msg =~ /\A\+(\d+)\z/m
		# Last N posts
		raise Huick::TooMuchError if $1.to_i > 100
		u = User.by_jid(m.from.stripped.to_s)
		say m.from, TemplateRenderer.render("lastposts", :posts => Huick.hide(Post.order("created_at DESC").limit($1.to_i), u), :user => u)
	elsif msg =~ /\ARC/m # formal commands
		ws = Shellwords::shellwords(msg)
		command = ws[1]
		args = ws[2..-1]
		
		unless $config[:admins].include? m.from.stripped.to_s
			raise Huick::NoPermissionError
		end
		
		case command
		when "reload-templates"
			TemplateRenderer.reset_templates
			say m.from, "Templates reset."
		when "ban"
			u = User.first(:conditions => {:jid => args[0]})
			u.banned = !u.banned
			u.save!
			say m.from, "User [un]banned."
		when "get"
			say m.from, "#{args[0].inspect} = #{GlobalSetting[args[0]].inspect}"
		when "set"
			GlobalSetting[args[0]] = args[1]
			say m.from, "#{args[0].inspect} = #{args[1].inspect}"
		end
	elsif msg =~ /\ADESCRIPTION (.*)/m
		# Set description
		u = Huick.require_user(m.from.stripped.to_s)
		u.description = $1
		u.save!
		say m.from, "Description updated."
	elsif msg =~ /\AEMAIL (.*)/m
		# Set email
		u = Huick.require_user(m.from.stripped.to_s)
		u.email = $1
		u.save!
		say m.from, "Email updated."
# 	elsif msg =~ /\ANICK (.*)/m
# 		# Set description
# 		u = Huick.require_user(m.from.stripped.to_s)
# 		u.username = $1
# 		if u.save
# 			say m.from, "Nick updated."
# 		else
# 			say m.from, "Can't set this nick. Only latin letters, numbers, _ and -, please. It also must be unique"
# 		end
	elsif msg =~ /\AREGISTER (.+)\z/
		username = $1.downcase.strip
		raise Huick::BadUsernameError if !(username =~ /^([a-z0-9_\-A-Z]+)$/) || username.size > 30
		raise Huick::AlreadyRegisteredError if User.by_jid(m.from.stripped.to_s)
		raise Huick::UsernameTakenError if User.find_by(username: username)
		if r = RegistrationRequest.find_by(:jid => m.from.stripped.to_s)
			r.username = username
			r.save!
		else
			r = RegistrationRequest.find_or_create_by(:jid => m.from.stripped.to_s, username: username)
			r.save!
		end
		say m.from, "Please visit #{$config[:site_url]}/validate?jid=#{CGI.escape(m.from.stripped.to_s)} to complete registration"
	else
		post_body = msg
	
		# Post!
		u = Huick.require_user(m.from.stripped.to_s)
		
		private_flag = if msg =~ /^P\s+/
			msg.gsub!(/^P\s+/,"")
			true
		else
			false
		end
		
		target_user = if msg =~ /^(?:@([a-zA-Z0-9\-_]+)\s+)/
			msg.gsub!(/^(?:@([a-zA-Z0-9\-_]+)\s+)/,"")
			User.find_by_username($1) or raise Huick::NoSuchUserError
		else
			nil
		end
		
		pst = if msg =~ %r#^\*(.*?)(?://\s+|\n)(.*)$#m
			Huick.add_post(u, m.from.resource, $2, $1.strip.split(/\s*,\s*/), target_user, private_flag)
		else
			Huick.add_post(u, m.from.resource, post_body, [], target_user, private_flag)
		end
	
		say m.from, "Post ##{pst.postid_text}: #{$config[:site_url]}/p/#{pst.postid_text}"
	end
	
	rescue Huick::NoSuchPostError
		say m.from, "Error: No such post"
	rescue Huick::NoSuchCommentError
		say m.from, "Error: No such comment"
	rescue Huick::NoSuchUserError
		say m.from, "Error: No such user"
	rescue Huick::NotRegisteredError
		say m.from, "Error: You are not registered. Say HELP for information."
	rescue Huick::AlreadySubscribedError
		say m.from, "Error: Already subscribed"
	rescue Huick::NotSubscribedError
		say m.from, "Error: Already not subscribed"
	rescue Huick::NoPermissionError
		say m.from, "Error: No permission"
	rescue Huick::AlreadyBlacklistedError
		say m.from, "Error: Already blacklisted"
	rescue Huick::NotBlacklistedError
		say m.from, "Error: Already not blacklisted"
	rescue Huick::AlreadyHiddenError
		say m.from, "Error: Already hidden"
	rescue Huick::NotHiddenError
		say m.from, "Error: Already not hidden"
	rescue Huick::SaemPersonError
		say m.from, "Error: U mad?"
	rescue Huick::UserBannedError
		say m.from, "Error: You're banned. 66"
	rescue Huick::AlreadyRegisteredError
		say m.from, "Error: You're already registered."
	rescue Huick::TooMuchError
		say m.from, "You're asking for too much. Please poumer."
	rescue Huick::PrivatePostError
		say m.from, "This post is private."
	rescue Huick::UsernameTakenError
		say m.from, "Username already taken"
	rescue Huick::TooFastError
		say m.from, "Not so fast, pal. Cool your jets."
	rescue Huick::BadUsernameError
		say m.from, "Bad username. /^([a-z0-9_\-A-Z]+)$/ && size < 30"
	rescue Exception => e
		ecode = DateTime.now.strftime("%s")
		say m.from, "Internal error, timestamp #{ecode}."
		File.open("error.log", "a") do |f|
			f.puts "[#{DateTime.now.to_s}] {#{ecode}} Exception #{e} raised at #{e.backtrace.join("    ")}"
		end
	end
end
EventMachine.run do
	timer = EventMachine::PeriodicTimer.new(1800) do
		say $config[:jid], "PING"
	end
	$client.run
end
